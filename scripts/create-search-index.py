#!/usr/bin/env python3

import os
import argparse
import re
import json
from pathlib import Path, PurePath
from collections import defaultdict
from urllib.parse import urlparse
from bs4 import BeautifulSoup, NavigableString
from rjsmin import jsmin

SITEMAP_PATH = '{public}/sitemap.xml'
SEARCH_ROOT = '{public}/_/js/search'
SEARCH_TEMPLATE_PATH_A = '{public}/main/{version}/search-results/index.html'
SEARCH_TEMPLATE_PATH_B = '{public}/main/{version}/search-results.html'
LOCAL_SEARCH_NAME = 'local-{version}.js'
REMOTE_SEARCH_NAME = '{version}.js'
FUNCTION_PATH = '{function_root}/search-{version_name}.js'
FUNCTION_URL = '/.netlify/functions/search-{version_name}'
JS_TEMPLATE_ROOT = '{scripts}/js'
LOCAL_SEARCH_TEMPLATE_NAME = 'search-local.js'
REMOTE_SEARCH_TEMPLATE_NAME = 'search-remote.js'
SEARCH_API_TEMPLATE_NAME = 'search-api.js'
SEARCH_RESULTS_TEMPLATE_NAME = 'search-results.hbs'

# AWS / Netlify functions do not support multiple dots in a function name.
# So anywhere the version must be used in the function name or url, you
# must convert the version to a name with version2name.
#
# IMPORTANT: must match src/helpers/version2name.js in the UI repo
def version2name(version):
    return version.replace('.', '_')

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--public', required=True,
                    help='Public site root')
parser.add_argument('-f', '--functions', required=True,
                    help='Netlify functions directory')
args = parser.parse_args()

scripts_path = Path(__file__).parent
js_template_root = Path(JS_TEMPLATE_ROOT.format(scripts=scripts_path))
public_path = Path(args.public)
sitemap_path = Path(SITEMAP_PATH.format(public=args.public))
search_root = Path(SEARCH_ROOT.format(public=args.public))
function_root = Path(args.functions)

if not public_path.exists():
    raise Exception("path %s does not exist" % public_path)
if not public_path.is_dir():
    raise Exception("path %s is not a directory" % public_path)
if not sitemap_path.exists():
    raise Exception("sitemap %s does not exist" % sitemap_path)
if not sitemap_path.is_file():
    raise Exception("sitemap %s is not readable" % sitemap_path)
# We create search_root if it does not exist
if not search_root.exists():
    os.makedirs(search_root)
if not search_root.is_dir():
    raise Exception("search root %s is not a directory" % search_root)
# We also create the functions root if it does not exist
if not function_root.exists():
    os.makedirs(function_root)
if not function_root.is_dir():
    raise Exception("function root %s is not a directory" % function_root)
if not js_template_root.is_dir():
    raise Exception("cannot find template root %s" % js_template_root)

# Load javascript templates
with open(js_template_root / LOCAL_SEARCH_TEMPLATE_NAME) as f:
    local_search_template = f.read()
with open(js_template_root / REMOTE_SEARCH_TEMPLATE_NAME) as f:
    remote_search_template = f.read()
with open(js_template_root / SEARCH_API_TEMPLATE_NAME) as f:
    search_api_template = f.read()

# Load handlebars templates
with open(js_template_root / SEARCH_RESULTS_TEMPLATE_NAME) as f:
    search_results_handlebars = f.read()

# Parse sitemap
with open(sitemap_path, 'r') as f:
    sitemap = BeautifulSoup(f.read(), 'html5lib')

# Parse paths from loc elements in sitemap
# <loc>https://domain/path/to/file</loc> => /path/to/file
paths = [urlparse(loc.get_text()).path for loc in sitemap.find_all('loc')]

# For each version, create JSON object containing all searchable content
# Assume location path is in format /component/version/rest
def squish(s):
    return re.sub('[ \t]+', ' ', re.sub('\n+', '\n', s)).strip()
def page2title(page):
    # Site title is appended to page title as " :: site title"
    # Strip site title before returning page title
    return squish(page.find('title').get_text()).split('::')[0].strip()
def page2category(page):
    # Ignore last item (title of the page)
    # Then category is (up to) first two items
    # Text is inside li span.item
    # If no categories at all, default to 'Home'
    # Finally join breadcrumbs together with '/'
    breadcrumbs = page.find('ul', {'id': 'breadcrumbs'})
    categories = [] if breadcrumbs is None else \
        [p.find('span', {'class', 'item'}).get_text()
         for p in breadcrumbs.find_all('li')[:-1][:2]]
    if len(categories) == 0:
        categories = ['Home']
    return ' / '.join(categories)
def page2content(page):
    # Content is all text inside article.doc
    return squish(page.find('article', {'class' : 'doc'}).get_text())
def load(path):
    version = Path(path).parts[2] # '/', 'component', 'version', 'rest', ...
    full_path = PurePath.joinpath(public_path, path.strip('/'))
    if not full_path.parts[-1].endswith('.html'):
        full_path = PurePath.joinpath(full_path, 'index.html')
    with open(full_path, 'r') as f:
        page = BeautifulSoup(f.read(), 'html5lib')
        return (version,
                page2title(page),
                page2category(page),
                page2content(page))
all_docs = defaultdict(list)
all_urls = defaultdict(dict)
next_id  = defaultdict(int)
for path in paths:
    version, title, category, content = load(path)
    id = next_id[version]
    next_id[version] += 1
    all_urls[version][id] = path
    all_docs[version] += [{
        'id': id,
        'title': title,
        'category': category,
        'content': content}]

# For each version, output a search index and search method
#
# The local search is a browser-based search where the index is in the browser.
#
# The remote search calls a search API implemented in a Netlify Function.
# In this case, the Netlify Function itself contains the index.
#
# ASSUMPTIONS FOR LOCAL SEARCH
#
# * rootPath is defined and set to a _relative_ path to the site root
#   in the browser _prior_ to loading this script
#
# * autocomplete.min.js (by Algolia) is already loaded
#
# ASSUMPTIONS FOR REMOTE SEARCH
#
# * Dependencies 'flexsearch' and 'handlebars' are loaded in package.json
#
for version in all_docs.keys():
    function_url = FUNCTION_URL.format(version_name=version2name(version))

    # Local (browser-based) search
    n = LOCAL_SEARCH_NAME.format(version=version)
    p = Path(search_root.joinpath(n))
    with open(p, 'w') as f:
        f.write(jsmin(local_search_template % {
            'urls': json.dumps(all_urls[version]),
            'docs': json.dumps(all_docs[version])}))

    # Remote (netlify function) search, browser calls remote API
    n = REMOTE_SEARCH_NAME.format(version=version)
    p = Path(search_root.joinpath(n))
    with open(p, 'w') as f:
        f.write(jsmin(remote_search_template % {
            'function_url': function_url}))

    # Netlify function for remote requests for search results
    #
    # TEMPLATE STRING
    #
    # When generating an HTML response from the netlify function
    # that serves search results, we need a template into which
    # to render results. We canibalize a page generated just for
    # this purpose (search-results.html). This page is not in the
    # nav and does not even need to be published. There should be
    # a copy of this page for each component version.
    #
    # We will replace the following:
    #
    # - placeholder attribute in the input box with class searchbox
    #   with a handlebars variable so that the term that was searched
    #   can be substitued
    #
    # - contents of the div with class search-results with handlebars
    #   code to create search results from json results
    #
    # We are dealing with strings-inside-strings here. For the
    # best chance at success, paste this string into javascript
    # surrounded by backticks, which supports multiline strings
    # and will parse ' and " as part of the string.
    #
    # And while it seems silly, it is easier to string-replace raw
    # html for the handlebars code into the rest of the template
    # rather than laboriously write all the bs4 code to add it
    #
    # OUTPUT FILE
    #
    # The search function itself is output to a file named for the
    # version. The function contains the search template and the
    # entire search index (for the version).
    #
    search_template_path_a = Path(SEARCH_TEMPLATE_PATH_A.format(
        public=args.public, version=version))
    search_template_path_b = Path(SEARCH_TEMPLATE_PATH_B.format(
        public=args.public, version=version))
    if search_template_path_a.is_file():
        search_template_path = search_template_path_a
    elif search_template_path_b.is_file():
        search_template_path = search_template_path_b
    else:
        raise Exception("search template cannot be found, make sure search-template.adoc exists at the module root in every version")
    with open(search_template_path, 'r') as f:
        template_soup = BeautifulSoup(f.read(), 'html5lib')
    search_results_element = template_soup.find('div', {'class': 'search-results'})
    search_results_element.clear()
    search_results_placeholder = '%(contents)s'
    search_results_element.insert(0, NavigableString(search_results_placeholder))
    searchbox = template_soup.find('input', {'class': 'searchbox'})
    searchbox['value'] = '{{term}}'
    template = str(template_soup)
    template = template.replace(search_results_placeholder,
                                search_results_handlebars)

    function_path = Path(FUNCTION_PATH.format(
        function_root=function_root, version_name=version2name(version)))
    with open(function_path, 'w') as f:
        f.write(jsmin(search_api_template % {
            'urls': json.dumps(all_urls[version]),
            'docs': json.dumps(all_docs[version]),
            'function_url': function_url,
            'template': template}))
