var FlexSearch = require("flexsearch");
var Handlebars = require("handlebars");

var RESULTS_PER_PAGE = 10

var template_source = `
%(template)s
`;

Handlebars.registerHelper('or', function(...args) {
  const numArgs = args.length
  if (numArgs === 3) return args[0] || args[1]
  if (numArgs < 3) throw new Error("'or' needs at least 2 arguments")
  args.pop()
  return args.some((it) => it)
});

var template = Handlebars.compile(template_source);

var urls = %(urls)s;

var docs = %(docs)s;

var index = new FlexSearch({
  "encode": "extra",
  doc: {
    id: "id",
    field: [
      "title",
      "category",
      "content"
    ]
  }
});

index.add(docs);

function parseSuggestion(suggestion) {
  return {
    'category': suggestion.category,
    'title': suggestion.title,
    'url': urls[suggestion.id]
  }
}

function generateBody(term, cursor, asHTML) {
  cursor.result = cursor.result.map(parseSuggestion)
  if (asHTML) {
    return template({term: term, cursor: cursor})
  } else {
    return JSON.stringify(cursor)
  }
}

function doSearch(term, page) {
  if (parseInt(page) == NaN) {
    page = '0'
  }
  cursor = index.search(term, {
    limit: RESULTS_PER_PAGE,
    page: page
  })
  n = cursor.next == null ? null : parseInt(cursor.next)
  p = parseInt(page)
  u = '%(function_url)s?term=' + encodeURI(term) + '&display=html&page='
  if (n !== null && n <= p) {
    // flexsearch wraps around; instead, just treat all pages past the
    // end as having no results
    cursor.result = []
  }
  if (p > 0) {
    cursor.prev_url = u + Math.max(0, (p - RESULTS_PER_PAGE))
    cursor.prev_label = 'Page ' + Math.max(1, Math.trunc(p / RESULTS_PER_PAGE))
  }
  if (n !== null && n > p) {
    cursor.next_url = u + n
    cursor.next_label = 'Page ' + (Math.trunc(n / RESULTS_PER_PAGE) + 1)
  }
  return cursor
}

exports.handler = function(event, context, callback) {
  term = event.queryStringParameters['term'] || ''
  asHTML = event.queryStringParameters['display'] === 'html' || false
  page = event.queryStringParameters['page'] || '0'
  callback(null, {
    statusCode: 200,
    headers: {
      'Content-Type': (asHTML ? 'text/html' : 'text/json')
    },
    body: generateBody(term, doSearch(term, page), asHTML)
  });
}
