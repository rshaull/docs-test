function fetchJSON(url, successCB, errorCB) {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', url, true)
  xhr.responseType = 'json'
  xhr.onload = function() {
    if (xhr.status == 200) {
      successCB(xhr.response.result)
    } else {
      errorCB(url, xhr.status, xhr.statusText)
    }
  }
  xhr.send()
}

function errorCB(url, status, statusText) {
  console.error('Error ' + status + ': ' + statusText + ' (' + url + ')')
}

function _doSearch(query, cb) {
  url = '%(function_url)s?term=' + encodeURIComponent(query)
  fetchJSON(url, cb, errorCB)
}

// doSearch includes a throttle and debounce of _doSearch
nextSearch = undefined
throttler  = undefined
debouncer  = undefined
function doNextSearch() {
  if (nextSearch) {
    nextSearch()
    nextSearch = undefined
  }
}
function doDebouncedSearch() {
  doNextSearch()
  debouncer = undefined
}
function doThrottledSearch() {
  doNextSearch()
  throttler = undefined
}
function doSearch(query, cb) {
  nextSearch = function() { _doSearch(query, cb) }
  if (!(throttler || debouncer)) {
    doNextSearch()
  }
  if (!throttler) {
    throttler = setTimeout(doThrottledSearch, 1000)
  }
  if (debouncer) {
    clearTimeout(debouncer)
    debouncer = undefined
  }
  debouncer = setTimeout(doDebouncedSearch, 500)
}

var display_prevCategory = undefined
function initDisplay(query, isEmpty) {
  // Hack: call before suggestions are rendered to initialize prevCategory
  display_prevCategory = undefined
  return ''
}
function displaySuggestion(suggestion) {
  s = ''
  if (display_prevCategory !== suggestion.category) {
    // A little defensive coding to display suggestions without a
    // category (sorted to the top) nicely. This shouldn't happen.
    if (display_prevCategory !== undefined || suggestion.category !== '') {
      category = autocomplete.escapeHighlightedString(suggestion.category)
      s += '<div class="na-category">' + category + '</div>'
    }
    display_prevCategory = suggestion.category
  }
  title = autocomplete.escapeHighlightedString(suggestion.title)
  s += '<div class="na-title">' + title + '</div>'
  return s
}

autocomplete('.searchbox', {
  hint: false,
  clearOnSelected: true,
  appendTo: '#autocomplete-container',
  cssClasses: {
    root: 'nuo-autocomplete',
    prefix: 'na'
  }
},
[
  {
    templates: {
      header: initDisplay, // hack alert
      suggestion: displaySuggestion
    },
    source: doSearch
  }
]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
  p = rootPath
  if (p.charAt(p.length - 1) !== '/') {
    p += '/'
  }
  p += suggestion.url
  window.location.href = p
});
