var urls = %(urls)s;

var docs = %(docs)s;

var index = new FlexSearch({
  "encode": "extra",
  doc: {
    id: "id",
    field: [
      "title",
      "category",
      "content"
    ]
  }
});

index.add(docs);

function hits(index, params) {
  return function doSearch(query, cb) {
    // TODO use params
    function sorter(a, b) {
      return a.category < b.category ? -1 :
             a.category > b.category ?  1 :
             a.title    < a.title    ? -1 :
             a.title    > a.title    ?  1 : 0;
    }
    results = index.search(query, { limit: 10, sort: sorter })
    cb(results)
  }
}

var display_prevCategory = undefined
function initDisplay(query, isEmpty) {
  // Hack: call before suggestions are rendered to initialize prevCategory
  display_prevCategory = undefined
  return ''
}
function displaySuggestion(suggestion) {
  s = ''
  if (display_prevCategory !== suggestion.category) {
    // A little defensive coding to display suggestions without a
    // category (sorted to the top) nicely. This shouldn't happen.
    if (display_prevCategory !== undefined || suggestion.category !== '') {
      category = autocomplete.escapeHighlightedString(suggestion.category)
      s += '<div class="na-category">' + category + '</div>'
    }
    display_prevCategory = suggestion.category
  }
  title = autocomplete.escapeHighlightedString(suggestion.title)
  s += '<div class="na-title">' + title + '</div>'
  return s
}

autocomplete('.searchbox', {
  hint: false,
  clearOnSelected: true,
  appendTo: '#autocomplete-container',
  cssClasses: {
    root: 'nuo-autocomplete',
    prefix: 'na'
  }
},
[
  {
    templates: {
      header: initDisplay, // hack alert
      suggestion: displaySuggestion
    },
    source: hits(index, { hitsPerPage: 5 })
  }
]).on('autocomplete:selected', function(event, suggestion, dataset, context) {
  p = rootPath
  if (p.charAt(p.length - 1) !== '/') {
    p += '/'
  }
  p += urls[suggestion.id]
  window.location.href = p
});
