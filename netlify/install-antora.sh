rm -r antora
curl -s https://gitlab.com/rshaull/antora/-/archive/auto-fill-links-and-cache/antora-auto-fill-links-and-cache.tar.gz | tar xvz
mv antora-auto-fill-links-and-cache antora
(cd antora && yarn install)
